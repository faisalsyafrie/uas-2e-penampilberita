package syafrie.faisal.uas_130_139

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.SeekBar
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class AdapterDataDetail(val dataDetail: List<HashMap<String,String>>, val mainActivity: Detail) :
    RecyclerView.Adapter<AdapterDataDetail.HolderDataDetail>(){
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterDataDetail.HolderDataDetail {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_detail,p0,false)
        return HolderDataDetail(v)


    }
    override fun getItemCount(): Int{
        return dataDetail.size
    }
	
    lateinit var preferences: SharedPreferences
    lateinit var preferences1: SharedPreferences
    val PREF_NAME = "setting"
    val PREF_NAME1 = "setting1"
    val FIELD_FONT_SIZE = "font_size"
    val FIELD_FONT_SIZE1 = "font_size"
    val FIELD_TEXT = "teks"
    val DEF_FONT_SIZE =12
    val DEF_FONT_SIZE1 =12
    val DEF_TEXT = "Hello World"

    inner class HolderDataDetail(v: View) : RecyclerView.ViewHolder(v){
        val txJudul = v.findViewById<TextView>(R.id.txtJudul)
        val txKategori = v.findViewById<TextView>(R.id.txtKategori)
        val txTanggal = v.findViewById<TextView>(R.id.txtTanggal)
        val txIsi = v.findViewById<TextView>(R.id.textView5)
        val photo = v.findViewById<ImageView>(R.id.imageView2)
        val cDetail = v.findViewById<ConstraintLayout>(R.id.cDetail)
    }

    val onSeek = object : SeekBar.OnSeekBarChangeListener{
        override fun onProgressChanged(seekBar: SeekBar?, progress:Int,
                                       fromuser:Boolean){

        }

        override fun onStartTrackingTouch(seekBar: SeekBar?){

        }

        override fun onStopTrackingTouch(seekBAr: SeekBar?){

        }

    }


    override fun onBindViewHolder(p0: AdapterDataDetail.HolderDataDetail, p1: Int) {
        val data : HashMap<String, String> = dataDetail.get(p1)
        p0.txJudul.setText(data.get("judul"))
        p0.txKategori.setText(data.get("nama_kategori"))
        p0.txIsi.setText(data.get("isi"))
        p0.txTanggal.setText(data.get("tanggal"))


        preferences = mainActivity.getSharedPreferences(PREF_NAME,Context.MODE_PRIVATE)
        preferences1 = mainActivity.getSharedPreferences(PREF_NAME1,Context.MODE_PRIVATE)
        p0.txJudul.textSize = preferences.getInt(FIELD_FONT_SIZE,DEF_FONT_SIZE).toFloat()
        p0.txIsi.textSize = preferences1.getInt(FIELD_FONT_SIZE1,DEF_FONT_SIZE1).toFloat()


//        p0.cLayout.setOnClickListener({
//
//            mainActivity.eddetail.setText(data.get("judul"))
//
//        })

        if(!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(p0.photo);


    }
}