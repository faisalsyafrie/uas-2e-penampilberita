package syafrie.faisal.uas_130_139

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences

import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.StrictMode
import android.view.View
import android.widget.SeekBar
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Response
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_detail.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

import kotlin.collections.HashMap


class Detail : AppCompatActivity(), View.OnClickListener {
    lateinit var mediaHelper : MediaHelper
    lateinit var beritaAdapter : AdapterDataDetail

    var daftarBerita = mutableListOf<HashMap<String,String>>()
    var daftarKategori = mutableListOf<String>()
    val url = "http://192.168.43.66/uas-2e-penampilberita-web/show_data.php"
    val url2 = "http://192.168.43.66/uas-2e-penampilberita-web/get_nama_kategori.php"
    val url3 = "http://192.168.43.66/uas-2e-penampilberita-web/query_upd_del_ins.php"
    val url4 = "http://192.168.43.66/uas-2e-penampilberita-web/upload.php"
    var imStr = ""
    var namafile = ""
    var fileUri = Uri.parse("")
    var pilihKategori =""

    var tahun = 0
    var bulan = 0
    var hari = 0
    var jam = 0
    var menit = 0
    var tanggal : String=""
    var varradio : String=""
    var namaBerita1 =""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        beritaAdapter = AdapterDataDetail(daftarBerita, this)
        mediaHelper = MediaHelper(this)
        val cal: Calendar = Calendar.getInstance()
        val outputFormat : DateFormat = SimpleDateFormat("yyyy-MM-dd")
        tanggal = outputFormat.format(cal.getTime())

        listDetail.layoutManager = LinearLayoutManager(this)
//        kategoriAdapter = ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line,
//            daftarKategori)
//        spinKategori.adapter = kategoriAdapter
//        spinKategori.onItemSelectedListener = itemSelected
//
//        imUpload.setOnClickListener(this)
//        btnUpdate.setOnClickListener(this)
//        btnDelete.setOnClickListener(this)
//        btnInsert.setOnClickListener(this)
		
		var paket : Bundle? = intent.extras
        namaBerita1 = paket?.get("X").toString()
        button4.setOnClickListener(this)

        listDetail.adapter = beritaAdapter



        try{
            val m = StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
            m.invoke(null)
        }catch(e:Exception){
            e.printStackTrace()
        }

    }

    fun uploadFile(){
        val request = object : StringRequest(Method.POST,url4,
            Response.Listener{response ->
                val jsonObject = JSONObject(response)
                val kode = jsonObject.getString("kode")
                if(kode.equals("000")){
                    Toast.makeText(this,"Upload foto sukses",Toast.LENGTH_LONG).show()
                }else{
                    Toast.makeText(this,"Upload foto gagal",Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener{error->
                Toast.makeText(this,"Tidak dapat terhubung ke server",Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String,String>{
                val hm = HashMap<String, String>()
                hm.put("imstr",imStr)
                hm.put("namafile",namafile)
                return hm
            }
        }
        val q =  Volley.newRequestQueue(this)
        q.add(request)
    }

//    val itemSelected = object : AdapterView.OnItemSelectedListener{
//        override fun onNothingSelected(parent: AdapterView<*>?) {
//            spinKategori.setSelection(0)
//            pilihKategori=daftarKategori.get(0)
//        }
//
//        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
//            pilihKategori=daftarKategori.get(position)
//        }
//
//    }



//	fun requestPermissions() = runWithPermissions(
//        Manifest.permission.WRITE_EXTERNAL_STORAGE,
//	Manifest.permission.CAMERA){
//		fileUri =mediaHelper.getOutputMediaFileUri()
//		val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
//		intent.putExtra(MediaStore.EXTRA_OUTPUT,fileUri)
//		startActivityForResult(intent, mediaHelper.getRcCamera())
//	}





    override fun onStart() {
        super.onStart()
        showDataBerita(namaBerita1)
    }

//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//        if(resultCode == Activity.RESULT_OK){
//            if(requestCode == mediaHelper.getRcGallery()){
//                imStr = mediaHelper.getBitmapToString(data!!.data!!, imageView2)
//            }
//        }
//    }
	
	lateinit var preferences: SharedPreferences
	lateinit var preferences1: SharedPreferences
    val PREF_NAME = "setting"
    val PREF_NAME1 = "setting1"
    val FIELD_FONT_SIZE = "font_size"
    val FIELD_FONT_SIZE1 = "font_size"
    val FIELD_TEXT = "teks"
    val DEF_FONT_SIZE =12
    val DEF_FONT_SIZE1 =12
    val DEF_TEXT = "Hello World"

    val onSeek = object : SeekBar.OnSeekBarChangeListener{
        override fun onProgressChanged(seekBar: SeekBar?, progress:Int,
                                       fromuser:Boolean){

        }

        override fun onStartTrackingTouch(seekBar: SeekBar?){

        }

        override fun onStopTrackingTouch(seekBAr: SeekBar?){

        }

    }



    fun showDataBerita(namaBerita : String) {
        val request = object : StringRequest(
            Request.Method.POST, url,
            Response.Listener { response ->
                daftarBerita.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length() - 1)) {
                    val jsonObject = jsonArray.getJSONObject(x)
                    var brt = HashMap<String, String>()
                    brt.put("judul", jsonObject.getString("judul"))
                    brt.put("isi", jsonObject.getString("isi"))
                    brt.put("nama_kategori", jsonObject.getString("nama_kategori"))
                    brt.put("url", jsonObject.getString("url"))
                    brt.put("tanggal", jsonObject.getString("tanggal"))
                    daftarBerita.add(brt)
                }
                beritaAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "Terjadi kesalahan koneksi ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams():MutableMap<String,String>{
                val hm = HashMap<String,String>()
                hm.put("nama",namaBerita)
                return hm
            }
        }


        val queue = Volley.newRequestQueue(this)
        queue.add(request)

    }

    override fun finish(){
        var intent= Intent()
        super.finish()
    }

    override fun onClick(v: View?) {
        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        preferences1 = getSharedPreferences(PREF_NAME1, Context.MODE_PRIVATE)
        val prefEditor = preferences.edit()
        val prefEditor1 = preferences1.edit()
        prefEditor.putInt(FIELD_FONT_SIZE,seekBar.progress)
        prefEditor1.putInt(FIELD_FONT_SIZE1,seekBar2.progress)
        prefEditor.commit()
        prefEditor1.commit()
        Toast.makeText(this,"Perubahan telah disimpan", Toast.LENGTH_SHORT).show()
        showDataBerita(namaBerita1)
    }
}