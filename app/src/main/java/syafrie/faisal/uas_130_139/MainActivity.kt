package syafrie.faisal.uas_130_139

import android.content.Intent
import android.os.Bundle
import android.os.StrictMode
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_berita.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.listBerita
import org.json.JSONArray


class  MainActivity : AppCompatActivity(), View.OnClickListener {
    lateinit var mediaHelper : MediaHelper
    lateinit var beritaAdapter : AdapterMain
    lateinit var kategoriAdapter : ArrayAdapter<String>
    var daftarBerita = mutableListOf<HashMap<String,String>>()
    var daftarKategori = mutableListOf<String>()
    val url = "http://192.168.43.66/uas-2e-penampilberita-web/show_data.php"
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.button ->{
                var intent = Intent(this, Berita::class.java)
                startActivity(intent)
            }
            R.id.button2 ->{
                var intent2 = Intent(this, Kategori::class.java)
                startActivity(intent2)
            }
            R.id.button3 ->{

                showDataBerita(eddetail.text.toString())
            }

        }
    }
	
	val RC_SUKSES : Int = 100

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        beritaAdapter = AdapterMain(daftarBerita, this)
        mediaHelper = MediaHelper(this)
        listBerita.layoutManager = LinearLayoutManager(this)
        listBerita.adapter = beritaAdapter

        button.setOnClickListener(this)
        button2.setOnClickListener(this)
        button3.setOnClickListener(this)

        try{
            val m = StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
            m.invoke(null)
        }catch(e:Exception){
            e.printStackTrace()
        }
    }
	
	 //override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
     //   super.onActivityResult(requestCode, resultCode, data)
      //  if(resultCode == Activity.RESULT_OK){
        //    if(requestCode == RC_SUKSES)
                //eddetail.setText(data?.extras?.getString("hasilKali"))
       // }
	//}

    fun showDataBerita(namaBerita : String) {
        val request = object : StringRequest(
            Request.Method.POST, url,
            Response.Listener { response ->
                daftarBerita.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length() - 1)) {
                    val jsonObject = jsonArray.getJSONObject(x)
                    var brt = HashMap<String, String>()
                    brt.put("judul", jsonObject.getString("judul"))
                    brt.put("isi", jsonObject.getString("isi"))
                    brt.put("nama_kategori", jsonObject.getString("nama_kategori"))
                    brt.put("url", jsonObject.getString("url"))
                    brt.put("tanggal", jsonObject.getString("tanggal"))
                    daftarBerita.add(brt)
                }
                beritaAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "Terjadi kesalahan koneksi ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams():MutableMap<String,String>{
                val hm = HashMap<String,String>()
                hm.put("nama",namaBerita)
                return hm
            }
        }


        val queue = Volley.newRequestQueue(this)
        queue.add(request)

    }

    override fun onStart() {
        super.onStart()
        showDataBerita("")
    }

    override fun finish(){
        var intent= Intent()
        super.finish()
    }

}
