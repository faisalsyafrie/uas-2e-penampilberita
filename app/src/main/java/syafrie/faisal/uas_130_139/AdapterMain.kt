package syafrie.faisal.uas_130_139

import android.content.Intent
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*

class AdapterMain(val dataMain: List<HashMap<String,String>>, val mainActivity: MainActivity) :
    RecyclerView.Adapter<AdapterMain.HolderMain>(){
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterMain.HolderMain {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_berita,p0,false)
        return HolderMain(v)
    }
    override fun getItemCount(): Int{
        return dataMain.size
    }

    val RC_SUKSES : Int = 100

    inner class HolderMain(v: View) : RecyclerView.ViewHolder(v){
        val txJudul = v.findViewById<TextView>(R.id.txJudul)
        val txKategori = v.findViewById<TextView>(R.id.txKategori)
        val txTanggal = v.findViewById<TextView>(R.id.txTanggal)
        val photo = v.findViewById<ImageView>(R.id.imageView)
        val cLayout = v.findViewById<ConstraintLayout>(R.id.cLayout)
    }

    override fun onBindViewHolder(p0: AdapterMain.HolderMain, p1: Int) {
        val data : HashMap<String, String> = dataMain.get(p1)
        p0.txJudul.setText(data.get("judul"))
        p0.txKategori.setText(data.get("nama_kategori"))
        p0.txTanggal.setText(data.get("tanggal"))

        if(p1.rem(2)== 0) p0.cLayout.setBackgroundColor(
            Color.rgb(230,245,240))
        else p0.cLayout.setBackgroundColor(Color.rgb(255,255,245))

        p0.cLayout.setOnClickListener({

            mainActivity.eddetail.setText(data.get("judul"))
            var intent3 = Intent(mainActivity, Detail::class.java)

            intent3.putExtra("X", data.get("judul"))
            mainActivity.startActivityForResult(intent3, RC_SUKSES)
        })

        if(!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(p0.photo);


    }

}